class Fighter {
    constructor(fighter){
        const { _id, source, ...clearFighter } = fighter;
        this.fighter = clearFighter;
    }

    getHitPower = () => {
        const criticalHitChance = Math.random() * (2 - 1) + 1;
        return this.fighter.attack * criticalHitChance;
    };

    getBlockPower = () => {
        const dodgeChance  = Math.random() * (2 - 1) + 1;
        return this.fighter.defence  * dodgeChance;
    };

    setHealth(health){
        this.fighter.health = health;
        return this;
    }

    setDamage(damage){
        this.fighter.health -= damage;
        return this;
    }
}