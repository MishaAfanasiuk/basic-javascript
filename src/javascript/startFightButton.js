import View from "./view";
import dataController from './store';

class StartFigthButton extends View {
    constructor(FightModal){
        super();
        this.FightModal = FightModal;

        this.figher1 = {};
        this.figher2 = {};

        const footer = this.createElement({
            tagName: 'div',
            className: 'footer'
        });

        this.figher1NameBlock = this.createElement({
            tagName: 'div',
            className: 'fighter-name'
        });

        this.figher1NameBlock.appendChild(document.createTextNode('Click to select fighter for left side'));

        this.figher2NameBlock = this.createElement({
            tagName: 'div',
            className: 'fighter-name'
        });

        this.figher2NameBlock.appendChild(document.createTextNode('Click to select fighter for right side'));


        this.figher1NameBlock.addEventListener('click', this.onChangeSide.bind(this, 'left'));
        this.figher2NameBlock.addEventListener('click', this.onChangeSide.bind(this, 'right'));

        this.instructionBlock = this.createElement({
            tagName: 'div',
            className: 'instruction',
        });

        const instruction =
            document.createTextNode('Click on left or right fighter block and than choose fighter for current side');

        this.instructionBlock.appendChild(instruction);

        footer.append(this.figher1NameBlock, this.instructionBlock, this.figher2NameBlock);

        return footer;
    }

    onChangeSide = (side) => {
        dataController.setSide(side);

        this.instructionBlock.innerHTML = `You are choosing fighter for ${side} side`;
    };

    checkFighers = () => {
        return dataController.areFightersChoosen;
    };

    onStart(){
        if (!this.checkPlayers()){
            return this.showAttentionModal();
        }

        return new this.FightModal();
    }


}

export default StartFigthButton;