import FighterView from './fighterView';

class fightView extends FighterView{
    constructor(fighter, handleClick) {
        super();

        this.createFighterModal(fighter, handleClick);
    }
    rootElement = document.getElementById('root');

    createFightView(fighter1, fighter2) {

        this.element = this.createElement({ tagName: 'div', className: 'fight-view' });

        const fighter1Block = this.createFighterBlock(fighter1);
        const fighter2Block = this.createFighterBlock(fighter2);

        this.rootElement.append(this.element, fighter1Block, fighter2Block);
    }

    createFighterBlock = (fighter) => {
        const { name, source } = fighter;

        const fighterBlock = this.createElement({
          tagName: 'div',
          className: 'fighter'
      });

      const imageBlock = this.createImage(this.fighter.source);
      const nameBlock = this.createName(name);

      const button = this.createElement({
          tagName: 'button',
          className: 'kick-button',
          attributes: {
              onclick: () => {},
              value: 'space'
          }
      });

        fighterBlock.append(nameBlock, imageBlock, button);
    };
}

export default FighterView;