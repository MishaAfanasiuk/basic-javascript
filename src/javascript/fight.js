const fight = (fighter1, fighter2) => {
    return () => {

    }
};

class fight {
    constructor(fighter1, fighter2){
        this.fighter1 = fighter1;
        this.fighter2 = fighter2;
    }

    nextTick = (attackSide) => {
        if (attackSide === 'left') {
            this.fighter2.setDamage(this.fighter1.getHitPower - this.fighter2.getBlockPower());
        }

        if (attackSide === 'right') {
            this.fighter1.setDamage(this.fighter2.getHitPower - this.fighter1.getBlockPower());
        }

        return this;
    }
}