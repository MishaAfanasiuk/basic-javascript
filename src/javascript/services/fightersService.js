import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id) {
    try {
      const endpoint = `details/fighter/${_id}.json`;
      const fighter = await callApi(endpoint, 'GET');

      return JSON.parse(atob(fighter.content))
    } catch (error) {
      throw error;
    }
  }
}

export default new FighterService();
