import View from './view';
import FighterView from './fighterView';
import FighterModalView from './fighterModalView';
import fightersService from './services/fightersService';
import dataController from './store';

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  getFighterFullInfo = async (_id) => {
    const fighterSavedInfo = this.fightersDetailsMap.get(_id);
    if (!fighterSavedInfo) {
      return await fightersService.getFighterDetails(_id);
    }
    return fighterSavedInfo;
  };

  showFighterModal = (fighter) => {
    const modal = new FighterModalView(fighter, this.closeModal);
  };

  closeModal = () => {
    console.log('modal is closed')
  };

  async handleFighterClick(event, {_id: fighterId}) {
    const fighter = await this.getFighterFullInfo(fighterId);

    this.fightersDetailsMap.set(fighter._id, fighter);

    if(!dataController.isFighterSelectEnable) {
      return dataController.setFighter(fighter)
    }

    this.showFighterModal(fighter);
    console.log('clicked');
    console.log(this.fightersDetailsMap);
    // get from map or load info and add to fightersMap
    // show modal with fighter info
    // allow to edit health and power in this modal
  }
}

export default FightersView;