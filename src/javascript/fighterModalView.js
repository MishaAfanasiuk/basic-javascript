import View from './view';

class FighterModalView extends View {
    constructor(fighter, handleClick) {
        super();

        this.createFighterModal(fighter, handleClick);
    }
    rootElement = document.getElementById('root');

    createFighterModal(fighter, handleOnCloseClick) {
        const { name, source, health, attack, defence } = fighter;
        console.log(fighter);

        this.element = this.createElement({ tagName: 'div', className: 'fighter-modal' });
        const modalWrapper = this.createElement({ tagName: 'div', className: 'modal-wrapper' });

        const nameInput = this.createElement({
            tagName: 'input',
            className: 'fighter-modal__input',
            attributes: {
                value: health,
                name: 'health',
                onchange: () => {console.log('input changed')}
            }
        });

        this.element.append(nameInput);
        this.element.addEventListener('click', event => handleOnCloseClick(event, fighter), false);
        this.rootElement.append(this.element, modalWrapper);
    }

    createName(name) {
        const nameElement = this.createElement({ tagName: 'span', className: 'name' });
        nameElement.innerText = name;

        return nameElement;
    }

    createImage(source) {
        const attributes = { src: source };
        const imgElement = this.createElement({
            tagName: 'img',
            className: 'fighter-image',
            attributes
        });

        return imgElement;
    }
}

export default FighterModalView;