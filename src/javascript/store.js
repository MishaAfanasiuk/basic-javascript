class DataController {
    constructor() {
        this.isFighterSelectEnable = false;
        this.firstFighter = {};
        this.secondFighter = {};
        this.currentSide = '';
    }


    toggleFighterSelect = (state) => {
        this.isFighterSelectEnable = state;
    };

    setFighter = (fighter) => {
        if (this.currentSide === 'left') {
            return this.firstFighter = fighter;
        }

        this.secondFighter = fighter;
        console.log(this);
    };

    areFightersChosen = () => {
        return this.firstFighter.name && this.secondFighter.name;
    };

    setSide = (side) => {
        this.currentSide = side;
        console.log(this)
    }

}

export default new DataController();